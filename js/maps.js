(function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r;
  i[r] = i[r] || function () {
    (i[r].q = i[r].q || []).push(arguments)
  }, i[r].l = 1 * new Date();
  a = s.createElement(o),
  m = s.getElementsByTagName(o)[0];
  a.async = 1;
  a.src = g;
  m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-71862530-1', 'auto');
ga('send', 'pageview');

function zoomToCountry(address) {

  if ((address != '') && geocoder) {
    geocoder.geocode({'address': address}, function (results, status) {
      google.maps.event.addListenerOnce(map, 'center_changed', centerChanged);
      google.maps.event.addListenerOnce(map, 'bounds_changed', centerChanged);
      if (status == google.maps.GeocoderStatus.OK) {
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          if ((results && results[0] && results[0].formatted_address) && (results[0].formatted_address == "Antarctica")) {
            map.setCenter(new google.maps.LatLng(-75, 0));
            map.setZoom(3);
          } else if (results && results[0] && results[0].geometry && results[0].geometry.viewport) {
            map.fitBounds(results[0].geometry.viewport);
          } else if (results && results[0] && results[0].geometry && results[0].geometry.bounds) {
            map.fitBounds(results[0].geometry.bounds);
          } else {
            var htmlStr = results[0].formatted_address + "" < br > "";
          }
        } else {
          alert("No results found");
        }
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
}
function centerChanged() {

  //                google.maps.event.addListenerOnce(map, 'center_changed', function () {
  //                    document.getElementById('info').innerHTML += "<br>center=" + map.getCenter().toUrlValue(6) + "<br>map.getBounds=" + map.getBounds().toUrlValue(6);
  //                });
  if (map.getCenter().lat() < -85) {
    google.maps.event.addListenerOnce(map, 'center_changed', function () {
      map.setZoom(3);
    });
    map.setCenter(new google.maps.LatLng(-75, 0));
  } else if (map.getCenter().lat() > 85) {
    google.maps.event.addListenerOnce(map, 'center_changed', function () {
      map.setZoom(3);
    });
    map.setCenter(new google.maps.LatLng(75, 0));
  }
}
var infoWindows = [];
google.load("maps", "3.7", {"other_params": "sensor=false&libraries=places,weather,panoramio&language=en"});
google.setOnLoadCallback(initialize);
function initialize(lat, long, locations, clickable) {
  if (lat == undefined || isNaN(lat)) {
    //                    lat = 21.827405;
    lat = 18.819557;
  }
  if (long == undefined || isNaN(long)) {
    //                    long =  77.515479;
    long = 63.936377;
  }
  //                alert(lat+","+long);
  if (locations == undefined) {
    locations = countries;
  }
  if (clickable == undefined) {
    clickable = true;
  }
  $('.country-locations').hide();
  //                $('#map').css('width', '100%');
  geocoder = new google.maps.Geocoder();
  //                var latlng = new google.maps.LatLng(14.058324,108.277199);
  var latlng = new google.maps.LatLng(lat, long);
  var mapOptions = {
    zoom: 3,
    scrollwheel: true,
    panControl: true,
    zoomControl: true,
    mapTypeControl: true,
    scaleControl: true,
    streetViewControl: true,
    overviewMapControl: true,
    overviewMapControlOptions: {opened: true},
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map"), mapOptions);
  pinMarkers(locations, clickable);
}

function pinMarkers(points, clickable) {
  markers = new Array();
  infoWindoMaps = new Array();
  j = 0;
  for (i in points) {
    markers[j] = new google.maps.Marker({
      map: map,
      draggable: false,
      position: new google.maps.LatLng(points[i].lat, points[i].long),
      title: points[i].title,
      clickable: clickable,
      icon: ""});
      google.maps.event.addListener(markers[j], 'click', function () {
        showCountryLocations(this);
      });
      j++;
    }
  }
  function findMessage(key) {
    for (i in coutryWiseLocations) {
      for (j in coutryWiseLocations[i]) {
        if (coutryWiseLocations[i][j].title == key) {
          return coutryWiseLocations[i][j].message;
        }
      }
    }
  }
  function  showCountryLocations(marker) {
    var msg = '';
    if (countriesList.indexOf(marker.title) != -1) {
      initialize(countries[marker.title].lat, countries[marker.title].long, coutryWiseLocations[marker.title], true);
      zoomToCountry(marker.title);
      msg = countries[marker.title].message;
      temp = countries[marker.title].title;
    } else {
      temp = marker.title;
      msg = findMessage(marker.title);
    }
    //                $('#map').css('width', '80%');
    $('.country-locations').show();


    if (temp.indexOf('_') != -1) {
      temp = temp.split('_').join(' ');

    }
    $('#countryName').html(temp);
    infoWindoMaps = new google.maps.InfoWindow({
      content: marker.title});
      //                        $('#countryFlag').html(points[i].flagHTMLCode);
      $('#message').html(msg);
      wgmp_closeAllInfoWindows();
      infoWindows.push(infoWindoMaps);
      infoWindoMaps.open(map, marker);
      google.maps.event.addListener(map, 'click', function () {
        try {
          infoWindoMaps.close();
          initialize();
        } catch (e) {

        }

      });
    }
    var countriesList = ['India', 'Bangladesh', 'Vietnam', 'Singapore', 'Nepal', 'Qatar', 'Cambodia', 'Egypt', 'Kazakhstan', 'Hong_Kong', 'Mauritius', 'Myanmar', 'Nigeria', 'Pakistan', 'Sri_Lanka', 'UAE'];
    var countries = {
      India: {
        title: 'India',
        lat: 28.6322444,
        long: 77.22072379999997,
        message: '<li><h3>Ahmedabad</h3> #301- 3rd Floor, Shivalik Point<br /> Mithakali Six Roads<br /> Navrangpura, Ahmedabad<br /> Gujarat- 380009, India<br /> TEL: +91 79 40099883</li><li><h3>Bangalore City Office</h3> # 410 Sajid Centre Point <br /> Opp ICICI Bank , Kamanahalli Main Road <br /> Bangalore – 560043</li><li><h3>Bangalore Cargo Office</h3> UNIT-7,III rd FLOOR, AIR INDIA,<br /> SATS Freight International,<br /> Bangaluru International Airport Limited,<br /> Devanahali, Bangaluru – 560300</li><li class="cleargap"></li><li><h3>Calicut Cargo Office</h3> No.11, ksie, calicut air cargo complex<br /> Logos complex, opp old intl airport,<br /> Calicut airport, Malappuram-673647</li><li><h3>Coimbatore City Office</h3> No 115, 41 A&B, Rajpriya “G” Avenue,<br /> Civil Aerodrome post, Avinashi Road,<br /> Coimbatore – 641014 , Tamil Nadu</li><li><h3>Coimbatore Cargo Office</h3> No.7, 41 A&B, Rajpriya “G” Avenue, <br /> Civil Aerodrome post, Avinashi Road, <br /> Coimbatore – 641014 , Tamil Nadu</li><li class="cleargap"></li><li><h3>Chennai City Office</h3> House #29 (Old#13),Dr. Nair Road,br /> T-Nagar, Chennai - 600 017, Tamil Nadu</li><li><h3>Chennai Cargo Office</h3> No.1,Alonkar Anugraha Complex, <br /> 501 MKN Road, Alandur, <br /> Chennai - 600 016, Tamil Nadu</li><li><h3>Cochin Cargo Office</h3> No.06, CPC Building,<br /> Air Cargo Complex<br /> Cochin International Airport<br /> Nudumbassery, Cochin - 683572</li><li class="cleargap"></li><li><h3>Delhi</h3> JMD Pacific Square (5th Floor)<br /> Sector-15, Part-II, Near 32 Mile Stone<br /> Gurgaon (Haryana)– 122001</li><li><h3>Delhi Cargo Office</h3> Room No.37, Import-II, Cargo Complex, <br /> IGI Airport, New Delhi – 110037</li><li><h3>Delhi (Warehouse)</h3> Kh. No: 606, House No: 674, <br /> Rangpuri Vilage,<br /> New Delhi-110037</li><li class="cleargap"></li><li><h3>Goa Cargo Office</h3> NO.05, Ground Floor<br /> Anand Arcade, Chicalim, Airport Road<br /> Vasco Da Gama, Vasco, Goa-403802</li><li><h3>Hyderabad</h3> 3rd Floor, Sharada Sadan, Road No.1, <br /> Banjara Hills, Hyderabad - 500 028</li><li><h3>Hyderabad Cargo Office</h3> No-10, First Floor, <br /> Menzis Air Cargo Complex<br /> Rajiv Gandhi International Airport <br /> Shamshabad – 501218</li><li class="cleargap"></li><li><h3>Jaipur</h3> Amrit Kalash Appartment, <br /> Flat No - A 205, Tonk Road, <br /> Near Kamal & Company, <br /> Jaipur.Tele: +91-9923663267</li><li><h3>Kolkata </h3> 5, Trinity Tower,4th Floor, <br /> 83 Topsia Road (South)<br /> Kolkata– 700014, West Bengal</li><li><h3>Ludhiana</h3> SBO 16, Chandigarh Road,<br /> Sector 32, Ludhiana – 141010</li><li class="cleargap"></li><li><h3>Mumbai</h3> B-804 ,“The Qube”<br /> C.T.S. No. 1498/A, M.V.Road, <br /> Near to International Airport,<br /> Marol, Andheri, (East),<br /> Mumbai – 400059.</li><li><h3>Mumbai (CFS)</h3> Sector No.8, Dronagiri, PO Box No.5,<br /> (JNPT),OPP. Bhendkhal Village,<br /> Taluka – Uran, Dist. Raigad, <r /> Navi Mumbai – 400707<br />ee: +91-22-6766 9400<br /> Fx +91-22-2747 0345</li><li><h3>Nagpur Cargo Office</h3> Plot No – J -13<br /> Scientific Co-operative Society<br /> West High Court Road, Laxmi Nagar<br /> Nagpur – 440025</li><li class="cleargap"></li><li><h3>Pune</h3> MGH Logistics Pvt Ltd, <br />601-A,Montreal Business Centre<br /> Pallord Farms,Baner<br />Pune-411 045,Maharashtra</li><li><h3>Tirupur</h3> 320,IInd floor,Muthu Plaza,<br /> Avinashi Road, Tirupur-641602.</li><li><h3>Trivandrum Cargo Office</h3> Tc no 34/1407 (6)<br /> Logos complex, opp old intl airport,<br /> Valiyathope junction, vallakadav,<br /> Trivandrum – 695008</li><li class="cleargap"></li><li><h3>Tuticorin</h3> 97H/31,Sri Mari, Ist Floor,<br /> Palayamkottai Road, <br /> Tuticorin - 628 008</li>'
      },
      Bangladesh: {
        title: 'Bangladesh',
        lat: 23.684994,
        long: 90.35633099999995,
        message: '<li><h3>Kawran Bazar, Dhaka</h3> Jahangir Tower (5th & 6th Floor),<br /> 10 Kazi Nazrul Islam Avenue,<br /> Karwan Bazar,<br /> Dhaka-1215.<br /> Tel:+8802-9125792-6 <br /> Fax: +8802-8115228, 8116522</li><li><h3>Gulshan, Dhaka</h3> Landmark ,<br /> 12-14 Gulshan North C/A, <br /> Gulshan Circle - 2, <br /> Dhaka.</li><li><h3>Chittagong</h3> Shafi Bhaban (4th floor),<br /> Sk. Mujib Road,<br /> Agrabad C/A,<br /> Chittagong.<br /> Tel : +88031-713147-8 , 721450<br /> Fax : +88031-710847</li><li class="cleargap"></li><li><h3>Khulna</h3> Munna Tower (5th Floor),<br /> 7 KDA Avenue, Khulna.<br /> Tel : + 88041-721381</li>'
      },
      Vietnam: {
        title: 'Vietnam',
        lat: 14.058324,
        long: 108.277199,
        message: '<li><h3>Ho Chi Minh</h3> Transmarine Logistics Co. Ltd.<br /> Mekong Tower (5th Floor)<br /> 235-241 Cong Hoa Street<br /> Ward 13, Tan Binh District<br /> Ho Chi Minh City, Vietnam<br /> T:  +84 38100030-80<br /> F:  +84 38100090</li><li><h3>Hanoi</h3> Transmarine Logistics Co. Ltd.<br /> Dao Duy Anh Tower (7th Floor)<br /> 09 Dao Duy Anh, Dong Da District,<br /> Hanoi, Vietnam.<br /> T:  +84 (4)35747459-62<br /> F:  +84 (4)35747463</li>'
      },
      Singapore: {
        title: 'Singapore',
        lat: 1.350423,
        long: 103.809927,
        message: '<li><h3>World Head Quarter</h3> 152 Beach Road #14-04 Gateway, East Singapore <br />(189721)</li>'
      },
      Nepal: {
        title: 'Nepal',
        lat: 27.70698,
        long: 85.311884,
        message: '<li><h3>Kathmandu City Office</h3> 332 Bhakti Chhen, Uttar Dhoka,<br /> Lazimpat<br /> Kathmandu Nepal <br /> Tel No: 4001071-76<br /> Fax No: 4001074</li><li><h3>Kathmandu Cargo Office</h3> Tribhuvan International Airport, <br /> Cargo Division, Sinamangal<br /> Kathmandu Nepal <br /> Tel No: 4499530<br /> Mobile Number: Mr. Kamal (Sales): 9801197020</li>'
      },
      Qatar: {
        title: 'Qatar',
        lat: 25.287175,
        long: 51.501294,
        message: '<li><h3>Doha</h3> WLL OFFICE 11-4, SECOND FLOOR,<br /> AL MUFTAH BUILDING,<br /> D- RING ROAD<br /> Doha , Qatar<br /> Mob:+974 33697224</li>'
      },
      Cambodia: {
        title: 'Cambodia',
        lat: 11.56655,
        long: 104.812583,
        message: '<li><h3>Phnom Penh</h3> Siri Tower, 6th Floor,<br /> (Opposite of Cambodia Chamber of Commerce)<br /> 104, Russian Federation Boulevard, <br /> Khan Toul Kork, Phnom Penh, <br /> Kingdom of Cambodia<br /> Tel.       : +855 (0)23998131, 23998132<br /> Fax       : +855 (0)23998006<br /> Mobile   : +855 (0)10734230</li>'
      },
      Egypt: {
        title: 'Egypt',
        lat: 26.820553,
        long: 30.802498000000014,
        message: '<li><h3>Alexandria</h3> CSA: Budget Cargo Egypt Ltd.<br /> 7th Floor, 11 Bani-EL-Abas Street<br /> Sultan Hussien,Azarita, <br /> Alexandria, Egypt .<br /> Tel: +20 3 4834058<br /> Mobile: +201012728639, +201096914000, +201000259363</li><li><h3>Cairo</h3> # 204 Cargo Agent Complex<br /> Cargo Village, Cairo International Airport<br /> Cairo, Egypt<br /> Mob: +201012728639, +201096914000, +201000259363</li>'
      },
      Kazakhstan: {
        title: 'Kazakhstan',
        lat: 43.205872,
        long: 76.953741,
        message: '<li><h3>Almaty</h3> Almaty Towers Business Center,<br /> South Tower, 5th floor<br /> 280 Bayzakov St.<br /> Almaty, Kazakhstan<br /> Mobile:+7701 801 98 26<br /> Tel.:       +7727 332 22 44<br /> +7727 328 07 55 (24/7)</li>'
      },
      Hong_Kong: {
        title: 'Hong_Kong',
        lat: 22.263421,
        long: 114.218472,
        message: '<li><h3>Hong Kong</h3> Suite No.3009, 30th Floor, Skyline Tower,<br /> No. 39, Wang Kwong Road,<br /> Kowloon Bay, Kowloon,<br /> Hong Kong S.A.R<br /> Tel:(852)22745964; 22745965<br /> Fax: (852)22745958<br /> H/P: (852)67181757</li>'
      },
      Mauritius: {
        title: 'Mauritius',
        lat: -20.348404,
        long: 57.55215199999998,
        message: '<li><h3>Port Louis</h3> 5th Floor, Office 1, Wing B,<br /> Ken Lee Building,<br /> 20, Edith Cavell Street,<br /> Port Louis, Mauritius<br /> Tel +(230)  2105832 / 96    <br /> Fax +(230)2106022</li>'
      },
      Myanmar: {
        title: 'Myanmar',
        lat: 16.982594,
        long: 96.114566,
        message: '<li><h3>Yangon</h3> 1707, Sakura Tower (17th Floor),<br /> 339, Bogyoke Aung San Road, <br /> Kyauktada Township, <br /> Yangon, Myanmar.<br /> Tel: + 95 1 - 255923 , 255924 <br /> Fax: + 95 1 - 255925</li>'
      },
      Nigeria: {
        title: 'Nigeria',
        lat: 9.081999,
        long: 8.675277000000051,
        message: '<li><h3>Lagos Island Office</h3> 38, Awolowo Road, South West, Ikoyi, Lagos.<br /> Phone:+2342770747,+2342770746</li><li><h3>Lagos Mainland Office</h3> 44, Opebi Road, Ikeja, Lagos.<br /> Phone:+2342950371,+2342950372</li><li><h3>Lagos State Office</h3> 63A Raymond Njkou Street<br /> South West Ikoyi, Lagos State</li><li class="cleargap"></li><li><h3>Abuja Office</h3> Suite F3, Hatlab Place, Plot 1952,<br /> Sokode Crescent, Wuse Zone 5, Abuja.</li><li><h3>Port-Harcourt </h3> 11, Stadium Road,<br /> Escape Center Building, Port Harcourt.</li>'
      },
      Pakistan: {
        title: 'Pakistan',
        lat: 33.5753184,
        long: 73.14307400000007,
        message: '<li><h3>Karachi</h3> Suite No.110, Ibrahim Trade tower,<br /> Maqboolabad CooperativeSocietySharah-e-Faisal Karachi-75400,<br /> Pakistan<br /> Tel: +92 21 34327521-6<br /> Fax: +92 21 34327569</li><li><h3>Lahore</h3> Suite # 402- B 4th Floor Eden Tower 82 – E – 1 Main BoulevardGulberg lll, <br /> Lahore. <br /> Tel : +92-42-5782301-3<br /> Fax :+92-42-5782304</li><li><h3>Faisalabad </h3> Suite # 4,3rd Floor, <br /> Legacy Tower Kohinoor City,<br /> Jarawala Road,<br /> Faisalabad. <br /> Tel : +92-51-8554491<br /> Fax : +92-51-8554492</li><li class="cleargap"></li><li><h3>Sialkot </h3> Suite # C–3, 2nd Floor,<br /> Jawad Centre,<br /> Sialkot.<br /> Tel : + 92-52-35733923<br /> Fax : + 92-52-3573924</li>'
      },
      Sri_Lanka: {
        title: 'Sri_Lanka',
        lat: 6.913363,
        long: 79.867629,
        message: '<li><h3>Colombo</h3> 15th Floor, World Trade Centre , <br /> East Tower, <br /> Colombo-1<br /> Tel:- +94 11 4 640 250-252<br /> Mob:- +94 777 309 880</li>'
      },
      UAE: {
        title: 'UAE',
        lat: 24.899328,
        long: 55.151519,
        message: '<li><h3>Dubai</h3> Unit # 2307, 23rd Floor<br /> PRISM Tower<br /> Business Bay, Dubai,U.A.E<br /> PO Box no:84790</li>'
      }
    };
    var coutryWiseLocations = {
      India: {
        //                    India: {
        //                    title: 'India',
        //                            lat: 28.6322444,
        //                            long: 77.22072379999997,
        //                            message: '<li><h3>Ahmedabad</h3> #301- 3rd Floor, Shivalik Point<br /> Mithakali Six Roads<br /> Navrangpura, Ahmedabad<br /> Gujarat- 380009, India<br /> TEL: +91 79 40099883</li><li><h3>Bangalore City Office</h3> # 410 Sajid Centre Point <br /> Opp ICICI Bank , Kamanahalli Main Road <br /> Bangalore – 560043</li><li><h3>Bangalore Cargo Office</h3> UNIT-7,III rd FLOOR, AIR INDIA,<br /> SATS Freight International,<br /> Bangaluru International Airport Limited,<br /> Devanahali, Bangaluru – 560300</li><li class="cleargap"></li><li><h3>Calicut Cargo Office</h3> No.11, ksie, calicut air cargo complex<br /> Logos complex, opp old intl airport,<br /> Calicut airport, Malappuram-673647</li><li><h3>Coimbatore City Office</h3> No 115, 41 A&B, Rajpriya “G” Avenue,<br /> Civil Aerodrome post, Avinashi Road,<br /> Coimbatore – 641014 , Tamil Nadu</li><li><h3>Coimbatore Cargo Office</h3> No.7, 41 A&B, Rajpriya “G” Avenue, <br /> Civil Aerodrome post, Avinashi Road, <br /> Coimbatore – 641014 , Tamil Nadu</li><li class="cleargap"></li><li><h3>Chennai City Office</h3> House #29 (Old#13),Dr. Nair Road,br /> T-Nagar, Chennai - 600 017, Tamil Nadu</li><li><h3>Chennai Cargo Office</h3> No.1,Alonkar Anugraha Complex, <br /> 501 MKN Road, Alandur, <br /> Chennai - 600 016, Tamil Nadu</li><li><h3>Cochin Cargo Office</h3> No.06, CPC Building,<br /> Air Cargo Complex<br /> Cochin International Airport<br /> Nudumbassery, Cochin - 683572</li><li class="cleargap"></li><li><h3>Delhi</h3> JMD Pacific Square (5th Floor)<br /> Sector-15, Part-II, Near 32 Mile Stone<br /> Gurgaon (Haryana)– 122001</li><li><h3>Delhi Cargo Office</h3> Room No.37, Import-II, Cargo Complex, <br /> IGI Airport, New Delhi – 110037</li><li><h3>Delhi (Warehouse)</h3> Kh. No: 606, House No: 674, <br /> Rangpuri Vilage,<br /> New Delhi-110037</li><li class="cleargap"></li><li><h3>Goa Cargo Office</h3> NO.05, Ground Floor<br /> Anand Arcade, Chicalim, Airport Road<br /> Vasco Da Gama, Vasco, Goa-403802</li><li><h3>Hyderabad</h3> 3rd Floor, Sharada Sadan, Road No.1, <br /> Banjara Hills, Hyderabad - 500 028</li><li><h3>Hyderabad Cargo Office</h3> No-10, First Floor, <br /> Menzis Air Cargo Complex<br /> Rajiv Gandhi International Airport <br /> Shamshabad – 501218</li><li class="cleargap"></li><li><h3>Jaipur</h3> Amrit Kalash Appartment, <br /> Flat No - A 205, Tonk Road, <br /> Near Kamal & Company, <br /> Jaipur.Tele: +91-9923663267</li><li><h3>Kolkata </h3> 5, Trinity Tower,4th Floor, <br /> 83 Topsia Road (South)<br /> Kolkata– 700014, West Bengal</li><li><h3>Ludhiana</h3> SBO 16, Chandigarh Road,<br /> Sector 32, Ludhiana – 141010</li><li class="cleargap"></li><li><h3>Mumbai</h3> B-804 ,“The Qube”<br /> C.T.S. No. 1498/A, M.V.Road, <br /> Near to International Airport,<br /> Marol, Andheri, (East),<br /> Mumbai – 400059.</li><li><h3>Mumbai (CFS)</h3> Sector No.8, Dronagiri, PO Box No.5,<br /> (JNPT),OPP. Bhendkhal Village,<br /> Taluka – Uran, Dist. Raigad, <r /> Navi Mumbai – 400707<br />ee: +91-22-6766 9400<br /> Fx +91-22-2747 0345</li><li><h3>Nagpur Cargo Office</h3> Plot No – J -13<br /> Scientific Co-operative Society<br /> West High Court Road, Laxmi Nagar<br /> Nagpur – 440025</li><li class="cleargap"></li><li><h3>Pune</h3> 1st Floor, Suyog Bhawan<br /> 197, Dhole Patil Road,<br /> Pune, Maharashtra - 411001</li><li><h3>Tirupur</h3> 320,IInd floor,Muthu Plaza,<br /> Avinashi Road, Tirupur-641602.</li><li><h3>Trivandrum Cargo Office</h3> Tc no 34/1407 (6)<br /> Logos complex, opp old intl airport,<br /> Valiyathope junction, vallakadav,<br /> Trivandrum – 695008</li><li class="cleargap"></li><li><h3>Tuticorin</h3> 97H/31,Sri Mari, Ist Floor,<br /> Palayamkottai Road, <br /> Tuticorin - 628 008</li>'
        //                    },
        Tamilnadu: {
          title: 'Tamilnadu',
          lat: 13.084866,
          long: 80.254278,
          message: '<li><h3>Chennai City Office</h3> House #29 (Old#13),Dr. Nair Road,<br /> T-Nagar, Chennai - 600 017, Tamil Nadu</li><li><h3>Chennai Cargo Office</h3> No.1,Alonkar Anugraha Complex, <br /> 501 MKN Road, Alandur, <br /> Chennai - 600 016, Tamil Nadu</li><li class="cleargap"></li><li><h3>Coimbatore City Office</h3> No 115, 41 A&B, Rajpriya “G” Avenue, <br /> Civil Aerodrome post, Avinashi Road,<br />oimbatore – 641014 , Tamil Nadu</li><li><h3>Coimbatore Cargo Office</h3> No.7, 41 A&B, Rajpriya “G” Avenue, <br /> Civil Aerodrome post, Avinashi Road, <br /> Coimbatore – 641014, Tamil Nadu</li><li><h3>Tirupur</h3> 320,IInd floor,Muthu Plaza,<br/> Avinashi Road, Tirupur-641602.</li><li><h3>Tuticorin</h3> 97H/31, Sri Mari, Ist Floor,<br /> Palayamkottai Road, <br /> Tuticorin - 628 008</li>'
        },
        Maharastra: {
          title: 'Maharastra',
          lat: 19.137995,
          long: 72.877073,
          message: '<li><h3>Mumbai</h3> B-804 ,“The Qube”<br /> C.T.S. No. 1498/A, M.V.Road, <br /> Near to International Airport,<br /> Marol, Andheri, (East),<br /> Mumbai – 400059.</li><li><h3>Mumbai (CFS)</h3> Sector No.8, Dronagiri, PO Box No.5,<br /> (JNPT),OPP. Bhendkhal Village,<br /> Taluka – Uran, Dist. Raigad,<br /> Navi Mumbai – 400707<br /> Tele: +91-22-6766 9400<br />ax: +91-22-2747 0345</li><li><h3>Nagpur Cargo Office</h3> Plot No – J -13<br /> Scientific Co-operative Society<br /> West High Court Road, Laxmi Nagar<br /> Nagpur – 440025</li><li><h3>Pune</h3> 601-A,Montreal Business Centre<br /> Pallord Farms,Baner<br /> Pune-411 045,India</li>'
        },
        Bangalore: {
          title: 'Bangalore',
          lat: 12.972584,
          long: 77.586942,
          message: '<li><h3>Bangalore City Office</h3> # 410 Sajid Centre Point <br /> Opp ICICI Bank , Kamanahalli Main Road <br /> Bangalore – 560043</li><li><h3>Bangalore Cargo Office</h3> UNIT-7,III rd FLOOR, AIR INDIA,<br /> SATS Freight International,<br /> Bangaluru International Airport Limited,<br /> Devanahali, Bangaluru – 560300</li>'
        },
        Kerala: {
          title: 'Kerala',
          lat: 11.258496,
          long: 75.780589,
          message: '<li><h3>Calicut Cargo Office</h3> No.11, ksie, calicut air cargo complex<br /> Logos complex, opp old intl airport,<br /> Calicut airport, Malappuram-673647</li><li><h3>Cochin Cargo Office</h3> No.06, CPC Building,<br /> Air Cargo Complex<br /> Cochin International Airport<br /> Nudumbassery, Cochin - 683572</li><li><h3>Trivandrum Cargo Office</h3> Tc no 34/1407 (6)<br /> Logos complex, opp old intl airport,<br /> Valiyathope junction, vallakadav,<br /> Trivandrum – 695008</li>'
        },
        Delhi_NCR: {
          title: 'Delhi_NCR',
          lat: 28.459783,
          long: 77.027535,
          message: '<li><h3>Gurgaon</h3> JMD Pacific Square (5th Floor)<br /> Sector-15, Part-II, Near 32 Mile Stone<br /> Gurgaon (Haryana)– 122001</li><li><h3>Delhi Cargo Office</h3> Room No.37, Import-II, Cargo Complex, <br /> IGI Airport, New Delhi – 110037</li><li><h3>Delhi (Warehouse)</h3> Kh. No: 606, House No: 674, <br /> Rangpuri Vilage,<br /> New Delhi-110037</li>'
        },
        Goa: {
          title: 'Goa',
          lat: 15.496091,
          long: 73.871897,
          message: '<li><h3>Goa Cargo Office</h3> NO.05, Ground Floor<br /> Anand Arcade, Chicalim, Airport Road<br /> Vasco Da Gama, Vasco, Goa-403802</li>'
        },
        Hyderabad: {
          title: 'Hyderabad',
          lat: 17.387255,
          long: 78.478032,
          message: '<li><h3>Hyderabad</h3> 3rd Floor, Sharada Sadan, Road No.1, <br /> Banjara Hills, Hyderabad - 500 028</li><li><h3>Hyderabad Cargo Office</h3> No-10, First Floor, <br /> Menzis Air Cargo Complex<br /> Rajiv Gandhi International Airport <br /> Shamshabad – 501218</li>'
        },
        Jaipur: {
          title: 'Jaipur',
          lat: 26.91331,
          long: 75.787799,
          message: '<li><h3>Jaipur</h3> Amrit Kalash Appartment, <br /> Flat No - A 205, Tonk Road, <br /> Near Kamal & Company, <br /> Jaipur.Tele: +91-9923663267</li>'
        },
        Kolkata: {
          title: 'Kolkata',
          lat: 22.574072,
          long: 88.354804,
          message: '<li><h3>Kolkata </h3> 5, Trinity Tower,4th Floor, <br /> 83 Topsia Road (South)<br /> Kolkata– 700014, West Bengal</li>'
        },
        Ludhiana: {
          title: 'Ludhiana',
          lat: 30.901448,
          long: 75.849109,
          message: '<li><h3>Ludhiana</h3> SBO 16, Chandigarh Road,<br /> Sector 32, Ludhiana – 141010</li>'
        },
        Ahmedabad: {
          title: 'Ahmedabad',
          lat: 23.022497,
          long: 72.569934,
          message: '<li><h3>Ahmedabad</h3> #301- 3rd Floor, Shivalik Point<br /> Mithakali Six Roads<br /> Navrangpura, Ahmedabad<br /> Gujarat- 380009, India<br /> TEL: +91 79 40099883</li>'
        }
      },
      Bangladesh: {
        //                            Bangladesh: {
        //                            title: 'Bangladesh',
        //                                    lat: 23.684994,
        //                                    long: 90.35633099999995,
        //                                    message: '<li><h3>Kawran Bazar, Dhaka</h3> Jahangir Tower (5th & 6th Floor),<br /> 10 Kazi Nazrul Islam Avenue,<br /> Karwan Bazar,<br /> Dhaka-1215.<br /> Tel:+8802-9125792-6 <br /> Fax: +8802-8115228, 8116522</li><li><h3>Gulshan, Dhaka</h3> Landmark ,<br /> 12-14 Gulshan North C/A, <br /> Gulshan Circle - 2, <br /> Dhaka.</li><li><h3>Chittagong</h3> Shafi Bhaban (4th floor),<br /> Sk. Mujib Road,<br /> Agrabad C/A,<br /> Chittagong.<br /> Tel : +88031-713147-8 , 721450<br /> Fax : +88031-710847</li><li class="cleargap"></li><li><h3>Khulna</h3> Munna Tower (5th Floor),<br /> 7 KDA Avenue, Khulna.<br /> Tel : + 88041-721381</li>'
        //                            },
        Dhaka: {
          title: 'Dhaka',
          lat: 23.762978,
          long: 90.43442,
          message: '<li><h3>Kawran Bazar, Dhaka</h3> Jahangir Tower (5th & 6th Floor),<br /> 10 Kazi Nazrul Islam Avenue,<br /> Karwan Bazar,<br /> Dhaka-1215.<br /> Tel:+8802-9125792-6 <br /> Fax: +8802-8115228, 8116522</li><li><h3>Gulshan, Dhaka</h3> Landmark ,<br /> 12-14 Gulshan North C/A, <br /> Gulshan Circle - 2, <br /> Dhaka.</li>'
        },
        Chittagong: {
          title: 'Chittagong',
          lat: 22.363725,
          long: 91.852793,
          message: '<li><h3>Chittagong</h3> Shafi Bhaban (4th floor),<br /> Sk. Mujib Road,<br /> Agrabad C/A,<br /> Chittagong.<br /> Tel : +88031-713147-8 , 721450<br /> Fax : +88031-710847</li>'
        },
        Khulna: {
          title: 'Khulna',
          lat: 23.513516,
          long: 89.021358,
          message: '<li><h3>Khulna</h3> Munna Tower (5th Floor),<br /> 7 KDA Avenue, Khulna.<br /> Tel : + 88041-721381</li>'
        }
      },
      Vietnam: {
        //                            Vietnam: {
        //                            title: 'Vietnam',
        //                                    lat: 14.058324,
        //                                    long: 108.277199,
        //                                    message: '<li><h3>Ho Chi Minh</h3> Transmarine Logistics Co. Ltd.<br /> Mekong Tower (5th Floor)<br /> 235-241 Cong Hoa Street<br /> Ward 13, Tan Binh District<br /> Ho Chi Minh City, Vietnam<br /> T:  +84 38100030-80<br /> F:  +84 38100090</li><li><h3>Hanoi</h3> Transmarine Logistics Co. Ltd.<br /> Dao Duy Anh Tower (7th Floor)<br /> 09 Dao Duy Anh, Dong Da District,<br /> Hanoi, Vietnam.<br /> T:  +84 (4)35747459-62<br /> F:  +84 (4)35747463</li>'
        //                            },
        Ho_Chi_Minh: {
          title: 'Ho_Chi_Minh',
          lat: 10.931523,
          long: 106.566594,
          message: '<li><h3>Ho Chi Minh</h3> Transmarine Logistics Co. Ltd.<br /> Mekong Tower (5th Floor)<br /> 235-241 Cong Hoa Street<br /> Ward 13, Tan Binh District<br /> Ho Chi Minh City, Vietnam<br /> T:  +84 38100030-80<br /> F:  +84 38100090</li>'
        },
        Hanoi: {
          title: 'Hanoi',
          lat: 21.019168,
          long: 105.830783,
          message: '<li><h3>Hanoi</h3> Transmarine Logistics Co. Ltd.<br /> Dao Duy Anh Tower (7th Floor)<br /> 09 Dao Duy Anh, Dong Da District,<br /> Hanoi, Vietnam.<br /> T:  +84 (4)35747459-62<br /> F:  +84 (4)35747463</li>'
        }
      },
      Singapore: {
        Singapore: {
          title: 'Singapore',
          lat: 1.350423,
          long: 103.809927,
          message: '<li><h3>World Head Quarter</h3> 152 Beach Road #14-04 Gateway, East Singapore <br />(189721)</li>'
        }
      },
      Nepal: {
        Kathmandu: {
          title: 'Kathmandu',
          lat: 27.70698,
          long: 85.311884,
          message: '<li><h3>Kathmandu City Office</h3> 332 Bhakti Chhen, Uttar Dhoka,<br /> Lazimpat<br /> Kathmandu Nepal <br /> Tel No: 4001071-76<br /> Fax No: 4001074</li><li><h3>Kathmandu Cargo Office</h3> Tribhuvan International Airport, <br /> Cargo Division, Sinamangal<br /> Kathmandu Nepal <br /> Tel No: 4499530<br /> Mobile Number: Mr. Kamal (Sales): 9801197020</li>'
        }
      },
      Qatar: {
        Doha: {
          title: 'Doha',
          lat: 25.287175,
          long: 51.501294,
          message: '<li><h3>Doha</h3> WLL OFFICE 11-4, SECOND FLOOR,<br /> AL MUFTAH BUILDING,<br /> D- RING ROAD<br /> Doha , Qatar<br /> Mob:+974 33697224</li>'
        }
      },
      Cambodia: {
        Phnom_Penh: {
          title: 'Phnom_Penh',
          lat: 11.56655,
          long: 104.812583,
          message: '<li><h3>Phnom Penh</h3> Siri Tower, 6th Floor,<br /> (Opposite of Cambodia Chamber of Commerce)<br /> 104, Russian Federation Boulevard, <br /> Khan Toul Kork, Phnom Penh, <br /> Kingdom of Cambodia<br /> Tel.       : +855 (0)23998131, 23998132<br /> Fax       : +855 (0)23998006<br /> Mobile   : +855 (0)10734230</li>'
        }
      },
      Egypt: {
        //                            Egypt: {
        //                            title: 'Egypt',
        //                                    lat: 26.820553,
        //                                    long: 30.802498000000014,
        //                                    message: '<li><h3>Alexandria</h3> CSA: Budget Cargo Egypt Ltd.<br /> 7th Floor, 11 Bani-EL-Abas Street<br /> Sultan Hussien,Azarita, <br /> Alexandria, Egypt .<br /> Tel: +20 3 4834058<br /> Mobile: +201273303678</li><li><h3>Cairo</h3> # 204 Cargo Agent Complex<br /> Cargo Village, Cairo International Airport<br /> Cairo, Egypt<br /> Mob: +2 01001732266</li>'
        //                            },
        Alexandria: {
          title: 'Alexandria',
          lat: 31.167649,
          long: 30.020106,
          message: '<li><h3>Alexandria</h3> CSA: Budget Cargo Egypt Ltd.<br /> 7th Floor, 11 Bani-EL-Abas Street<br /> Sultan Hussien,Azarita, <br /> Alexandria, Egypt .<br /> Tel: +20 3 4834058<br /> Mobile: +201273303678</li>'
        },
        Cairo: {
          title: 'Cairo',
          lat: 30.05947,
          long: 31.255715,
          message: '<li><h3>Cairo</h3> # 204 Cargo Agent Complex<br /> Cargo Village, Cairo International Airport<br /> Cairo, Egypt<br /> Mob: +2 01001732266</li>'
        }
      },
      Kazakhstan: {
        Almaty: {
          title: 'Almaty',
          lat: 43.205872,
          long: 76.953741,
          message: '<li><h3>Almaty</h3> Almaty Towers Business Center,<br /> South Tower, 5th floor<br /> 280 Bayzakov St.<br /> Almaty, Kazakhstan<br /> Mobile:+7701 801 98 26<br /> Tel.:       +7727 332 22 44<br /> +7727 328 07 55 (24/7)</li>'
        }
      },
      Hong_Kong: {
        Hong_Kong: {
          title: 'Hong_Kong',
          lat: 22.263421,
          long: 114.218472,
          message: '<li><h3>Hong Kong</h3> Suite No.3009, 30th Floor, Skyline Tower,<br /> No. 39, Wang Kwong Road,<br /> Kowloon Bay, Kowloon,<br /> Hong Kong S.A.R<br /> Tel:(852)22745964; 22745965<br /> Fax: (852)22745958<br /> H/P: (852)67181757</li>'
        }
      },
      Mauritius: {
        Port_Louis: {
          title: 'Port_Louis',
          lat: -20.348404,
          long: 57.55215199999998,
          message: '<li><h3>Port Louis</h3> 5th Floor, Office 1, Wing B,<br /> Ken Lee Building,<br /> 20, Edith Cavell Street,<br /> Port Louis, Mauritius<br /> Tel +(230)  2105832 / 96    <br /> Fax +(230)2106022</li>'
        }
      },
      Myanmar: {
        Yangon: {
          title: 'Yangon',
          lat: 16.982594,
          long: 96.114566,
          message: '<li><h3>Yangon</h3> 1707, Sakura Tower (17th Floor),<br /> 339, Bogyoke Aung San Road, <br /> Kyauktada Township, <br /> Yangon, Myanmar.<br /> Tel: + 95 1 - 255923 , 255924 <br /> Fax: + 95 1 - 255925</li>'
        }
      },
      Nigeria: {
        Lagos: {
          title: 'Lagos',
          lat: 6.508575,
          long: 3.272478,
          message: '<li><h3>Lagos Island Office</h3> 38, Awolowo Road, South West, Ikoyi, Lagos.<br /> Phone:+2342770747,+2342770746</li><li><h3>Lagos Mainland Office</h3> 44, Opebi Road, Ikeja, Lagos.<br /> Phone:+2342950371,+2342950372</li><li><h3>Lagos State Office</h3> 63A Raymond Njkou Street<br /> South West Ikoyi, Lagos State</li>'
        },
        Abuja: {
          title: 'Abuja',
          lat: 9.061766,
          long: 7.312633,
          message: '<li><h3>Abuja Office</h3> Suite F3, Hatlab Place, Plot 1952,<br /> Sokode Crescent, Wuse Zone 5, Abuja.</li>'
        },
        Port_Harcourt: {
          title: 'Port_Harcourt',
          lat: 4.808767,
          long: 7.027363,
          message: '<li><h3>Port-Harcourt </h3> 11, Stadium Road,<br /> Escape Center Building, Port Harcourt.</li>'
        }
      },
      Pakistan: {
        Karachi: {
          title: 'Karachi',
          lat: 24.956112,
          long: 67.001485,
          message: '<li><h3>Karachi</h3> Suite No.110, Ibrahim Trade tower,<br /> Maqboolabad CooperativeSocietySharah-e-Faisal Karachi-75400,<br /> Pakistan<br /> Tel: +92 21 34327521-6<br /> Fax: +92 21 34327569</li>'
        },
        Lahore: {
          title: 'Lahore',
          lat: 31.442675,
          long: 74.278816,
          message: '<li><h3>Lahore</h3> Suite # 402- B 4th Floor Eden Tower 82 ? E ? 1 Main BoulevardGulberg lll, <br /> Lahore. <br /> Tel : +92-42-5782301-3<br /> Fax :+92-42-5782304</li>'
        },
        Faisalabad: {
          title: 'Faisalabad',
          lat: 31.486357,
          long: 73.122071,
          message: '<li><h3>Faisalabad </h3> Suite # 4,3rd Floor, <br /> Legacy Tower Kohinoor City,<br /> Jarawala Road,<br /> Faisalabad. <br /> Tel : +92-51-8554491<br /> Fax : +92-51-8554492</li>'
        },
        Sialkot: {
          title: 'Sialkot',
          lat: 32.506027,
          long: 74.531158,
          message: '<li><h3>Sialkot </h3> Suite # C–3, 2nd Floor,<br /> Jawad Centre,<br /> Sialkot.<br /> Tel : + 92-52-35733923<br /> Fax : + 92-52-3573924</li>'
        }
      },
      Sri_Lanka: {
        Colombo: {
          title: 'Colombo',
          lat: 6.913363,
          long: 79.867629,
          message: '<li><h3>Colombo</h3> 15th Floor, World Trade Centre , <br /> East Tower, <br /> Colombo-1<br /> Tel:- +94 11 4 640 250-252<br /> Mob:- +94 777 309 880</li>'
        }
      },
      UAE: {
        Dubai: {
          title: 'Dubai',
          lat: 24.899328,
          long: 55.151519,
          message: '<li><h3>Dubai</h3> Unit # 2307, 23rd Floor<br /> PRISM Tower<br /> Business Bay, Dubai,U.A.E<br /> PO Box no:84790</li>'
        }
      }
    };
    
    function wgmp_closeAllInfoWindows() {
      for (var i = 0; i < infoWindows.length; i++) {
        infoWindows[i].close();
      }
      infoWindows = [];
    }
    $(document).ready(function () {
      var currentLocation = window.location.pathname.substring(window.location.pathname.lastIndexOf("/") + 1);
      $('.menu-item a[href="' + currentLocation + '"]').append('<div class="active-menu"></div>');
    });
